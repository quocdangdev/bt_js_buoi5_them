const KHU_VUC_A = "A";
const KHU_VUC_B = "B";
const KHU_VUC_C = "C";
const DOI_TUONG_1 = "1";
const DOI_TUONG_2 = "2";
const DOI_TUONG_3 = "3";

function tinhDiemKhuVuc(diemkhucvuc) {
  switch (diemkhucvuc) {
    case "A": {
      return 2;
    }
    case "B": {
      return 1;
    }
    case "C": {
      return 0.5;
    }
    case "X": {
      return 0;
    }
  }
}
function tinhDiemDoiTuong(diemDoituong) {
  if (diemDoituong == DOI_TUONG_1) {
    return 2.5;
  } else if (diemDoituong == DOI_TUONG_2) {
    return 1.5;
  } else if (diemDoituong == DOI_TUONG_3) {
    return 1;
  } else {
    return 0;
  }
}

function ketQua() {
  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  var khuVuc = document.getElementById("txt-khu-vuc").value;
  var doiTuong = document.getElementById("txt-doi-tuong").value;
  var diemMonThu1 = document.getElementById("diem-mon-thu1").value * 1;
  var diemMonThu2 = document.getElementById("diem-mon-thu2").value * 1;
  var diemMonThu3 = document.getElementById("diem-mon-thu3").value * 1;

  var objectPoint = tinhDiemDoiTuong(doiTuong);
  console.log(" objectPoint:", objectPoint);
  var diemCacKhuVuc = tinhDiemKhuVuc(khuVuc);
  console.log(" diemCacKhuVuc:", diemCacKhuVuc);
  var diemTong3Mon = diemMonThu1 + diemMonThu2 + diemMonThu3;
  console.log(" diemTong3Mon:", diemTong3Mon);

  var diemTongKet = diemTong3Mon + objectPoint + diemCacKhuVuc;
  console.log("🚀diemTongKet", diemTongKet);

  if (diemMonThu1 <= 0 || diemMonThu2 <= 0 || diemMonThu3 <= 0) {
    document.getElementById(
      "result"
    ).innerHTML = ` <p>bạn đã rớt vì có môn 1 bé hơn hoặc bằng 0, với điểm tổng kết là:${diemTongKet}</p> `;
  } else if (diemTongKet >= diemChuan) {
    document.getElementById(
      "result"
    ).innerHTML = `<p>bạn đã đậu với điểm tổng kết là:${diemTongKet}</p>`;
  } else {
    document.getElementById(
      "result"
    ).innerHTML = `<p>bạn rớt với tổng điểm là:${diemTongKet}</p>`;
  }
}

// bài 2:
function tinhTien50KwDau(kw) {
  if (kw <= 50) {
    return 500;
  }
}

function tinhTien50KwKe(kw) {
  if (kw > 50 && kw <= 100) {
    return 650;
  }
}
function tinhTien100KwKe(kw) {
  if (kw > 100 && kw <= 200) {
    return 850;
  }
}
function tinhTien150KwKe(kw) {
  if (kw > 200 && kw <= 350) {
    return 1100;
  }
}
function tinhTienKwConLai(kw) {
  if (kw > 350) {
    return 1300;
  }
}

function tienDien() {
  var name = document.getElementById("txt-ho-ten").value;
  console.log("🚀 ~ file: index.js ~ line 86 ~ tienDien ~ name", name);
  var soKw = document.getElementById("txt-so-kw").value * 1;
  var giaTien50kwDau = tinhTien50KwDau(soKw);
  console.log("🚀giaTien50kwDau", giaTien50kwDau);
  var giaTien50kwKe = tinhTien50KwKe(soKw);
  console.log("🚀  giaTien50kwKe", giaTien50kwKe);
  var giaTien100kwKe = tinhTien100KwKe(soKw);
  console.log("🚀 giaTien100kwKe", giaTien100kwKe);
  var giaTien150kwKe = tinhTien150KwKe(soKw);
  console.log("🚀  giaTien150kwKe", giaTien150kwKe);
  var giaTienkwConLai = tinhTienKwConLai(soKw);
  console.log("🚀giaTienkwConLai", giaTienkwConLai);

   var tienDien;
  if (soKw > 0 && soKw <= 50) {
    var tienDien = giaTien50kwDau * soKw;
    console.log("🚀 tienDien", tienDien)
    document.getElementById("result1").innerHTML=`họ tên :${name} ; tiền điện của bạn là : ${tienDien} </p>` 
  } else if (soKw > 50 && soKw <= 100) {
    var tienDien = 50*500+(soKw-50)*650;
    console.log("tienDien", tienDien)
    document.getElementById("result1").innerHTML=`họ tên :${name} ; tiền điện của bạn là : ${tienDien} </p>` 
  } else if (soKw > 100 && soKw <= 200) {
    tienDien = 50*500+50*650+(soKw-100)*850
    console.log("🚀 tienDien", tienDien);
    document.getElementById("result1").innerHTML=`họ tên :${name} ; tiền điện của bạn là : ${tienDien} </p>` 
  } else if (soKw > 200 && soKw <= 350) {
    tienDien = 50*500+50*650+100*850+(soKw-200)*1100
    console.log("🚀 tienDien", tienDien);
    document.getElementById("result1").innerHTML=`họ tên :${name} ; tiền điện của bạn là : ${tienDien} </p>` 
  } else {
    tienDien = 50*500+50*650+100*850+150*1100+(soKw-350)*1300
    console.log("🚀 tienDien", tienDien);
    document.getElementById("result1").innerHTML=`họ tên :${name} ; tiền điện của bạn là : ${tienDien} </p>` 
  }
}
